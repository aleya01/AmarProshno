<!DOCTYPE html>

<html>
    <head>
        <title>Amar Proshno</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/signUp.css">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="../index.php">
                        <img src="../assets/images/red.png" class="titleimage" height="50" width="60">
                        <span class="titleAmar">amar</span><span class="titleProshno">proshno</span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav ">
                        <li><a href="#">Question</a></li>
                        <li><a href="#">Jobs</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Tags</a></li>
                        <li><a href="#">Users</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </form>

                    <ul class="nav navbar-nav singUp ">
                        <li><a href="../Views/logIn.php"><span  class="login">Log In</span></a></li>
                        <li class="btnstyle"><a href="../Views/signUp.php"><button type="button" class="btn btn-primary">Sing Up</button></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logSign">
                        <ul id="select">
                            <li class="active"><a href="../Views/login.php">Log in</a> </li>|
                            <li><a href="#">Sign up</a> </li>
                        </ul>
                    </div>
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-12">
                    <div class="CreateTitle">
                        <p >Create your Amar Proshno account. It's free and only takes a minute.</p>
                    </div>
                </div>
            </div><br>


            <div class="row">
                <div class="col-md-12">
                    <div class="signUpForm">
                        <div class="SignTable">
                            <br>
                                <div class="form-group">
                                    <label for="name">Display Name</label>
                                    <input type="name" class="form-control" id="name" placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email (required, but never shown)</label>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password</label>
                                    <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                                </div>
                                <div class="btnn">
                                    <button type="submit" class="btn btn-primary">Sign Up</button>
                                    <p class="regTxt">By registering, you agree to the <sapn class="privacy">privacy policy</sapn> and <sapn class="privacy">terms of service</sapn>.</p>
                                </div> <br>
                            </form>
                        </div>
                    </div>
                    <div class="ALreadyLoged">
                        <br><p> Already have an account? <a href="../Views/login.php" class="AlrdyLog">Log in</a></p> <br>

                    </div>
                </div>
            </div><br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer">

                    </div>
                </div>
            </div>
        </div>





        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>
