<!DOCTYPE html>

<html>
<head>
    <title> Amar Proshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/question.css">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="../index.php">
                <img src="../assets/images/red.png" class="titleimage" height="50" width="60">
                <span class="titleAmar">amar</span><span class="titleProshno">proshno</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav ">
                <li><a href="#">Question</a></li>
                <li><a href="#">Jobs</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Tags</a></li>
                <li><a href="#">Users</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>

            <ul class="nav navbar-nav singUp ">
                <li><a href="../Views/login.php"><span  class="login">Log In</span></a></li>
                <li class="btnstyle"><a href="../Views/signUp.php"><button type="button" class="btn btn-primary">Sing Up</button></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">


    <div class="row">
        <div class="col-md-12">
            <div class="content">
                <div class="leftContent">
                    <div class="topQue">
                        <p></p>
                    </div>

                    <div class="Quetable">
                        <p class="InputBox"><input type="text" placeholder="Please write your que" name="question" class="form-control queInputBox"></p>
                        <div class="textArea">
                            <textarea></textarea>
                            <p class="button"><input type="submit" name="submit" class="btn btn-primary btnnn" value="Post"></p>
                        </div>

                    </div>
                </div>


                <div class="rightContent">
                    <div class="ddd">

                    </div>

                    <div class="Advertise">

                    </div>
                    <div class="Advertise">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="footer">

            </div>
        </div>
    </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
